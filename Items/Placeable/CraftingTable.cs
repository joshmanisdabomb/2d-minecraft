﻿using Terraria.ModLoader;

namespace Minecraft.Items.Placeable
{
    class CraftingTable : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Crafting Table");
            Tooltip.SetDefault("Used for crafting Minecraft items.");
        }

        public override void SetDefaults()
        {
            item.width = 28;
            item.height = 28;
            item.maxStack = 99;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.useStyle = 1;
            item.consumable = true;
            item.createTile = mod.TileType("CraftingTable");
        }
    }
}
