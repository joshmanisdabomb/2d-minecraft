﻿using Terraria;
using Terraria.ModLoader;

namespace Minecraft.Items.Armor
{
    [AutoloadEquip(EquipType.Body)]
    class VillagerRobe : ModItem
    {
        public override void SetDefaults()
        {
            item.width = 18;
            item.height = 14;
            item.rare = 1;
            item.vanity = true;
            item.value = Item.sellPrice(0, 0, 3, 0);
        }

        public override void SetMatch(bool male, ref int equipSlot, ref bool robes)
        {
            robes = false;
            //equipSlot = mod.GetEquipSlot("VillagerRobe_Body", EquipType.Body);
        }

        public override void DrawHands(ref bool drawHands, ref bool drawArms)
        {
            drawHands = false;
            drawArms = false;
        }

        public override bool DrawBody()
        {
            return false;
        }
    }
}
