﻿using Terraria;
using Terraria.ModLoader;

namespace Minecraft.Items.Armor
{
    [AutoloadEquip(EquipType.Head)]
    class VillagerMask : ModItem
    {
        public override void SetDefaults()
        {
            item.width = 12;
            item.height = 18;
            item.rare = 1;
            item.vanity = true;
            item.value = Item.sellPrice(0, 0, 3, 0);
        }
    }
}
