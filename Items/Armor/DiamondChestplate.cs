﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Armor
{
    [AutoloadEquip(EquipType.Body)]
    class DiamondChestplate : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Chestplate");
        }

        public override void SetDefaults()
        {
            item.defense = 7;
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, (40 / 12) * 30, 0);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 35);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
