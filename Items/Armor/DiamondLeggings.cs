﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Armor
{
    [AutoloadEquip(EquipType.Legs)]
    class DiamondLeggings : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Leggings");
        }

        public override void SetDefaults()
        {
            item.defense = 6;
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, (30 / 12) * 30, 0);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 30);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
