﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Armor
{
    [AutoloadEquip(EquipType.Head)]
    class DiamondHelmet : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Helmet");
        }

        public override void SetDefaults()
        {
            item.defense = 6;
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, (50 / 12) * 30, 0);
        }

        public override bool IsArmorSet(Item head, Item body, Item legs)
        {
            return body.type == mod.ItemType("DiamondChestplate") && legs.type == mod.ItemType("DiamondLeggings");
        }

        public override void UpdateArmorSet(Player player)
        {
            player.setBonus = "+4 defense";
            player.statDefense += 4;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 25);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
