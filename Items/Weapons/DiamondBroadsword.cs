using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Weapons
{
	public class DiamondBroadsword : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Diamond Broadsword");
		}

		public override void SetDefaults()
		{
			item.damage = 18;
			item.melee = true;
			item.width = 24;
			item.height = 28;
			item.useTime = 21;
			item.useAnimation = 19;
			item.useStyle = 1;
			item.knockBack = 5;
            item.value = Item.sellPrice(0, 0, (18 / 12) * 30, 0);
            item.rare = 1;
			item.UseSound = SoundID.Item1;
			item.autoReuse = false;
            item.scale = 1.075F;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.Diamond, 8);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
