﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Weapons
{
    class DiamondBow : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Bow");
        }

        public override void SetDefaults()
        {
            item.damage = 14;
            item.noMelee = true;
            item.hammer = 55;
            item.width = 12;
            item.height = 28;
            item.useTime = 24;
            item.useAnimation = 24;
            item.useStyle = 5;
            item.knockBack = 1.5F;
            item.value = Item.sellPrice(0, 0, (14 / 12) * 30, 0);
            item.rare = 1;
            item.UseSound = SoundID.Item5;
            item.autoReuse = false;
            item.scale = 1.3F;
            item.shoot = 1;
            item.shootSpeed = 6.8F;
            item.ranged = true;
            item.useAmmo = AmmoID.Arrow;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 7);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
