﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Weapons
{
    class DiamondShortsword : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Shortsword");
        }

        public override void SetDefaults()
        {
            item.damage = 16;
            item.melee = true;
            item.width = 24;
            item.height = 28;
            item.useTime = 12;
            item.useAnimation = 9;
            item.useStyle = 3;
            item.knockBack = 4;
            item.value = Item.sellPrice(0, 0, (14 / 12) * 30, 0);
            item.rare = 1;
            item.UseSound = SoundID.Item1;
            item.autoReuse = false;
            item.scale = 1F;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 7);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
