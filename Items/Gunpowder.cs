﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items
{
    class Gunpowder : ModItem
    {
        public override void SetStaticDefaults()
        {
            Tooltip.SetDefault("Better than Explosive Powder");
        }

        public override void SetDefaults()
        {
            item.width = 12;
            item.height = 20;
            item.maxStack = 999;
            item.value = Item.sellPrice(0, 0, 0, 30);
            item.rare = 0;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(this, 15);
            recipe.AddIngredient(ItemID.SandBlock, 12);
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(ItemID.Explosives, 1);
            recipe.AddRecipe();
        }
    }
}
