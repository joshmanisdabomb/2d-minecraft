﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Tools
{
    class DiamondPickaxe : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Pickaxe");
            Tooltip.SetDefault("Able to mine Hellstone");
        }

        public override void SetDefaults()
        {
            item.damage = 8;
            item.melee = true;
            item.pick = 70;
            item.width = 24;
            item.height = 28;
            item.useTime = 12;
            item.useAnimation = 17;
            item.useStyle = 1;
            item.knockBack = 2;
            item.value = Item.sellPrice(0, 0, (20 / 12) * 30, 0);
            item.rare = 1;
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
            item.scale = 1.075F;
            item.useTurn = true;
            item.tileBoost = 0;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 12);
            recipe.AddIngredient(ItemID.Wood, 4);
            recipe.anyWood = true;
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
