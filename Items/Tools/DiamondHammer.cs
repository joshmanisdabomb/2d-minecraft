﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Tools
{
    class DiamondHammer : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Hammer");
        }

        public override void SetDefaults()
        {
            item.damage = 11;
            item.melee = true;
            item.hammer = 55;
            item.width = 24;
            item.height = 28;
            item.useTime = 20;
            item.useAnimation = 25;
            item.useStyle = 1;
            item.knockBack = 5.75F;
            item.value = Item.sellPrice(0, 0, (16 / 12) * 30, 0);
            item.rare = 1;
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
            item.scale = 1.3F;
            item.useTurn = true;
            item.tileBoost = 0;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 10);
            recipe.AddIngredient(ItemID.Wood, 3);
            recipe.anyWood = true;
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
