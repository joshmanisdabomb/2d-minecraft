﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.Items.Tools
{
    class DiamondAxe : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Diamond Axe");
        }

        public override void SetDefaults()
        {
            item.damage = 9;
            item.melee = true;
            item.axe = 13;
            item.width = 24;
            item.height = 28;
            item.useTime = 16;
            item.useAnimation = 23;
            item.useStyle = 1;
            item.knockBack = 5;
            item.value = Item.sellPrice(0, 0, (16 / 12) * 30, 0);
            item.rare = 1;
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
            item.scale = 1.175F;
            item.useTurn = true;
            item.tileBoost = 0;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Diamond, 9);
            recipe.AddIngredient(ItemID.Wood, 3);
            recipe.anyWood = true;
            recipe.AddTile(mod.TileType("CraftingTable"));
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
