using Minecraft.Currency;
using Terraria;
using Terraria.GameContent.UI;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft
{
	class Minecraft : Mod
    {
        public static int EmeraldCurrencyId;

        public Minecraft()
        {

        }

        public override void Load()
        {
            EmeraldCurrencyId = CustomCurrencyManager.RegisterCurrency(new EmeraldCurrency(ItemID.Emerald, 999L));
        }
	}
}
