﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Terraria;
using Terraria.Audio;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace Minecraft.NPCs
{
    class Creeper : ModNPC, IMinecraftNPC
    {
        private int timer = 0;
        private int sound = 0;
        public const int FuseTime = 68;
        public const int ExplosionDamage = 110;

        public LegacySoundStyle[] IdleSounds { get { return new LegacySoundStyle[] { }; } }

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Creeper");
            Main.npcFrameCount[npc.type] = Main.npcFrameCount[NPCID.GoblinScout] * 2;
        }

        public override void SetDefaults()
        {
            npc.width = 22;
            npc.height = 40;
            npc.aiStyle = 3;
            npc.damage = 0;
            npc.defense = 3;
            npc.lifeMax = 80 * (Main.expertMode ? 2 : 1);
            npc.HitSound = SoundID.NPCHit32;
            npc.DeathSound = SoundID.NPCDeath26;
            npc.knockBackResist = Main.expertMode ? 0.5F : 1F;
            npc.value = 60f;
            //aiType = NPCID.AngryBones;
            animationType = NPCID.GoblinScout;
            banner = npc.type;
            bannerItem = mod.ItemType("CreeperBanner");
        }

        public override float SpawnChance(NPCSpawnInfo spawnInfo)
        {
            return Math.Max(SpawnCondition.OverworldNightMonster.Chance * 0.15f, SpawnCondition.Underground.Chance * 0.1f);
        }

        public override void AI()
        {
            this.AI_003_Fighters(31);
        }

        public override void PostAI()
        {
            base.PostAI();
            if (npc.Distance(Main.player[npc.target].Center) < 96f && !Main.player[npc.target].dead)
            {
                timer += 1;
                if (sound <= 0)
                {
                    Main.PlaySound(SoundID.Item66, npc.position);
                }
                sound = 40;
                if (timer >= FuseTime)
                {
                    Explode(5);
                }
            }
            else
            {
                timer = Math.Max(0, timer - 1);
                sound = Math.Max(0, sound - 1);
            }
        }

        public override void FindFrame(int frameHeight)
        {
            base.FindFrame(frameHeight);
            int blinkSpeed = Math.Max((FuseTime - timer), 4);
            if (timer % blinkSpeed > 0 && timer % blinkSpeed <= (blinkSpeed / 2)) {
                npc.frame.Y += Main.npcFrameCount[NPCID.GoblinScout] * frameHeight;
            }
        }

        public override void NPCLoot()
        {
            switch (Main.rand.Next(0,70))
            {
                case 0:
                    Item.NewItem(npc.getRect(), ItemID.CreeperMask, 1);
                    break;
                case 1:
                    Item.NewItem(npc.getRect(), ItemID.CreeperShirt, 1);
                    break;
                case 2:
                    Item.NewItem(npc.getRect(), ItemID.CreeperPants, 1);
                    break;
                default:
                    Item.NewItem(npc.getRect(), mod.ItemType("Gunpowder"), Main.rand.Next(1,4));
                    break;
            }
        }

        private void Explode(int radius)
        {
            // Play explosion sound
            Main.PlaySound(SoundID.Item14, npc.position);

            // Smoke Dust spawn
            for (int i = 0; i < 50; i++)
            {
                int dustIndex = Dust.NewDust(new Vector2(npc.position.X, npc.position.Y), npc.width, npc.height, 31, 0f, 0f, 100, default(Color), 2f);
                Main.dust[dustIndex].velocity *= 1.4f;
            }

            // Fire Dust spawn
            for (int i = 0; i < 80; i++)
            {
                int dustIndex = Dust.NewDust(new Vector2(npc.position.X, npc.position.Y), npc.width, npc.height, 6, 0f, 0f, 100, default(Color), 3f);
                Main.dust[dustIndex].noGravity = true;
                Main.dust[dustIndex].velocity *= 5f;
                dustIndex = Dust.NewDust(new Vector2(npc.position.X, npc.position.Y), npc.width, npc.height, 6, 0f, 0f, 100, default(Color), 2f);
                Main.dust[dustIndex].velocity *= 3f;
            }

            // Large Smoke Gore spawn
            for (int g = 0; g < 2; g++)
            {
                int goreIndex = Gore.NewGore(new Vector2(npc.position.X + (float)(npc.width / 2) - 24f, npc.position.Y + (float)(npc.height / 2) - 24f), default(Vector2), Main.rand.Next(61, 64), 1f);
                Main.gore[goreIndex].scale = 1.5f;
                Main.gore[goreIndex].velocity.X = Main.gore[goreIndex].velocity.X + 1.5f;
                Main.gore[goreIndex].velocity.Y = Main.gore[goreIndex].velocity.Y + 1.5f;
                goreIndex = Gore.NewGore(new Vector2(npc.position.X + (float)(npc.width / 2) - 24f, npc.position.Y + (float)(npc.height / 2) - 24f), default(Vector2), Main.rand.Next(61, 64), 1f);
                Main.gore[goreIndex].scale = 1.5f;
                Main.gore[goreIndex].velocity.X = Main.gore[goreIndex].velocity.X - 1.5f;
                Main.gore[goreIndex].velocity.Y = Main.gore[goreIndex].velocity.Y + 1.5f;
                goreIndex = Gore.NewGore(new Vector2(npc.position.X + (float)(npc.width / 2) - 24f, npc.position.Y + (float)(npc.height / 2) - 24f), default(Vector2), Main.rand.Next(61, 64), 1f);
                Main.gore[goreIndex].scale = 1.5f;
                Main.gore[goreIndex].velocity.X = Main.gore[goreIndex].velocity.X + 1.5f;
                Main.gore[goreIndex].velocity.Y = Main.gore[goreIndex].velocity.Y - 1.5f;
                goreIndex = Gore.NewGore(new Vector2(npc.position.X + (float)(npc.width / 2) - 24f, npc.position.Y + (float)(npc.height / 2) - 24f), default(Vector2), Main.rand.Next(61, 64), 1f);
                Main.gore[goreIndex].scale = 1.5f;
                Main.gore[goreIndex].velocity.X = Main.gore[goreIndex].velocity.X - 1.5f;
                Main.gore[goreIndex].velocity.Y = Main.gore[goreIndex].velocity.Y - 1.5f;
            }

            //Break guff
            int minTileX = (int)(npc.position.X / 16f - (float)radius);
            int maxTileX = (int)(npc.position.X / 16f + (float)radius);
            int minTileY = (int)(npc.position.Y / 16f - (float)radius);
            int maxTileY = (int)(npc.position.Y / 16f + (float)radius);
            if (minTileX < 0)
            {
                minTileX = 0;
            }
            if (maxTileX > Main.maxTilesX)
            {
                maxTileX = Main.maxTilesX;
            }
            if (minTileY < 0)
            {
                minTileY = 0;
            }
            if (maxTileY > Main.maxTilesY)
            {
                maxTileY = Main.maxTilesY;
            }
            bool canKillWalls = false;
            for (int x = minTileX; x <= maxTileX; x++)
            {
                for (int y = minTileY; y <= maxTileY; y++)
                {
                    float diffX = Math.Abs((float)x - npc.position.X / 16f);
                    float diffY = Math.Abs((float)y - npc.position.Y / 16f);
                    double distance = Math.Sqrt((double)(diffX * diffX + diffY * diffY));
                    if (distance < (double)radius && Main.tile[x, y] != null && Main.tile[x, y].wall == 0)
                    {
                        canKillWalls = true;
                        break;
                    }
                }
            }
            for (int i = minTileX; i <= maxTileX; i++)
            {
                for (int j = minTileY; j <= maxTileY; j++)
                {
                    float diffX = Math.Abs((float)i - npc.position.X / 16f);
                    float diffY = Math.Abs((float)j - npc.position.Y / 16f);
                    double distanceToTile = Math.Sqrt((double)(diffX * diffX + diffY * diffY));
                    if (distanceToTile < (double)radius)
                    {
                        bool canKillTile = true;
                        if (Main.tile[i, j] != null && Main.tile[i, j].active())
                        {
                            canKillTile = true;
                            if (Main.tileDungeon[(int)Main.tile[i, j].type] || Main.tile[i, j].type == 88 || Main.tile[i, j].type == 21 || Main.tile[i, j].type == 26 || Main.tile[i, j].type == 107 || Main.tile[i, j].type == 108 || Main.tile[i, j].type == 111 || Main.tile[i, j].type == 226 || Main.tile[i, j].type == 237 || Main.tile[i, j].type == 221 || Main.tile[i, j].type == 222 || Main.tile[i, j].type == 223 || Main.tile[i, j].type == 211 || Main.tile[i, j].type == 404)
                            {
                                canKillTile = false;
                            }
                            if (!Main.hardMode && Main.tile[i, j].type == 58)
                            {
                                canKillTile = false;
                            }
                            if (!TileLoader.CanExplode(i, j))
                            {
                                canKillTile = false;
                            }
                            if (canKillTile)
                            {
                                WorldGen.KillTile(i, j, false, false, false);
                                if (!Main.tile[i, j].active() && Main.netMode != 0)
                                {
                                    NetMessage.SendData(17, -1, -1, null, 0, (float)i, (float)j, 0f, 0, 0, 0);
                                }
                            }
                        }
                        if (canKillTile)
                        {
                            for (int x = i - 1; x <= i + 1; x++)
                            {
                                for (int y = j - 1; y <= j + 1; y++)
                                {
                                    if (Main.tile[x, y] != null && Main.tile[x, y].wall > 0 && canKillWalls && WallLoader.CanExplode(x, y, Main.tile[x, y].wall))
                                    {
                                        WorldGen.KillWall(x, y, false);
                                        if (Main.tile[x, y].wall == 0 && Main.netMode != 0)
                                        {
                                            NetMessage.SendData(17, -1, -1, null, 2, (float)x, (float)y, 0f, 0, 0, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Kill Creeper
            npc.life = 0;
            npc.HitEffect();
            npc.active = false;

            //Damage NPCs and Players
            foreach (NPC n in Main.npc)
            {
                if (n.active)
                {
                    float distance = n.Distance(npc.position);
                    if (distance <= (radius + 1) * 16)
                    {
                        n.StrikeNPC(ExplosionDamage + (Main.expertMode ? 80 : 0) + Main.rand.Next(0, 10), 10, 0, false, true, false);
                    }
                }
            }
            foreach (Player p in Main.player)
            {
                if (p.active && !p.dead)
                {
                    float distance = p.Distance(npc.position);
                    if (distance <= (radius + 1) * 16)
                    {
                        p.Hurt(PlayerDeathReason.ByNPC(npc.whoAmI), ExplosionDamage + (Main.expertMode ? 80 : 0) + Main.rand.Next(0, 10), 0, false, false, false, -1);
                    }
                }
            }
        }
    }
}
