﻿using Terraria;
using Terraria.Audio;
using Terraria.ID;
using Terraria.Localization;
using Terraria.ModLoader;

namespace Minecraft.NPCs
{
    [AutoloadHead]
    class Villager : ModNPC, IMinecraftNPC
    {
        public override string Texture { get { return "Minecraft/NPCs/Villager"; } }

        public override string[] AltTextures { get { return new[] { "Minecraft/NPCs/Villager_Alt_1", "Minecraft/NPCs/Villager_Alt_2", "Minecraft/NPCs/Villager_Alt_3" }; } }

        public LegacySoundStyle[] IdleSounds { get { return new LegacySoundStyle[] { }; } }

        public override bool Autoload(ref string name)
        {
            name = "Villager";
            return mod.Properties.Autoload;
        }

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Villager");
            Main.npcFrameCount[npc.type] = 25;
            NPCID.Sets.ExtraFramesCount[npc.type] = 9;
            NPCID.Sets.AttackFrameCount[npc.type] = 4;
            NPCID.Sets.HatOffsetY[npc.type] = 4;
        }

        public override void SetDefaults()
        {
            npc.townNPC = true;
            npc.friendly = true;
            npc.homeless = true;
            npc.width = 18;
            npc.height = 40;
            npc.aiStyle = 7;
            npc.damage = 0;
            npc.defense = 0;
            npc.lifeMax = 250;
            npc.HitSound = SoundID.NPCHit1;
            npc.DeathSound = SoundID.NPCDeath1;
            npc.knockBackResist = 0.5f;
            animationType = NPCID.Guide;
        }

        public override bool CanTownNPCSpawn(int numTownNPCs, int money)
        {
            for (int k = 0; k < 255; k++)
            {
                Player player = Main.player[k];
                if (player.active)
                {
                    foreach (Item item in player.inventory)
                    {
                        if (item.type == ItemID.Emerald)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool CheckConditions(int left, int right, int top, int bottom)
        {
            for (int x = left; x <= right; x++)
            {
                for (int y = top; y <= bottom; y++)
                {
                    if (Main.tile[x, y].nactive() && Main.tile[x, y].type == mod.TileType("CraftingTable"))
                    {
                        return base.CheckConditions(left, right, top, bottom);
                    }
                }
            }
            return false;
        }

        public override string TownNPCName()
        {
            return "Testificate";
        }

        public override string GetChat()
        {
            int hms = Main.rand.Next(1, 4);
            string chat = "";
            for (int i = 0; i < hms; i++)
            {
                chat += "h" + new string('m', Main.rand.Next(2, 6)) + " ";
            }
            return chat;
        }

        public override void SetChatButtons(ref string button, ref string button2)
        {
            button = Language.GetTextValue("LegacyInterface.28");
        }

        public override void OnChatButtonClicked(bool firstButton, ref bool shop)
        {
            if (firstButton)
            {
                shop = true;
            }
        }

        public override void SetupShop(Chest shop, ref int nextSlot)
        {
            shop.item[nextSlot].SetDefaults(mod.ItemType("CraftingTable"));
            shop.item[nextSlot].shopCustomPrice = 10;
            shop.item[nextSlot++].shopSpecialCurrency = Minecraft.EmeraldCurrencyId;
            //if (WorldGen.TownManager.HasRoomQuick(npc.type))
            //{
            //    shop.item[nextSlot++].SetDefaults(ItemID.WoodenBoomerang);
            //}
        }

        public override void NPCLoot()
        {
            Item.NewItem(npc.getRect(), Main.rand.NextBool() ? mod.ItemType("VillagerMask")  : mod.ItemType("VillagerRobe"));
        }
    }
}
