﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Terraria;
using Terraria.DataStructures;
using Terraria.ModLoader;

namespace Minecraft.Players
{
    class MinecraftPlayer : ModPlayer
    {
        public static readonly PlayerLayer VillagerRobeLegs = new PlayerLayer("Minecraft", "VillagerRobeLegs", PlayerLayer.Legs, delegate (PlayerDrawInfo drawInfo)
        {
            Player drawPlayer = drawInfo.drawPlayer;

            Mod mod = ModLoader.GetMod("Minecraft");

            if (drawPlayer.body != mod.GetItem("VillagerRobe").item.bodySlot) return;
            if (drawPlayer.invis || drawPlayer.dead) return;

            Texture2D texture = mod.GetTexture("Items/Armor/VillagerRobe_Legs");
            Vector2 drawPosition = new Vector2((float)((int)(drawPlayer.position.X - Main.screenPosition.X - (float)(drawPlayer.bodyFrame.Width / 2f) + (float)(drawPlayer.width / 2f))), (float)((int)(drawPlayer.position.Y - Main.screenPosition.Y + (float)drawPlayer.height - (float)drawPlayer.bodyFrame.Height + 4f + drawPlayer.gfxOffY - drawPlayer.mount.PlayerOffset)));

            DrawData data = new DrawData(texture, drawPosition + drawPlayer.bodyPosition + drawInfo.bodyOrigin, drawPlayer.legFrame, drawInfo.middleArmorColor, drawPlayer.bodyRotation, drawInfo.bodyOrigin, 1f, drawInfo.spriteEffects, 0);
            data.shader = drawInfo.bodyArmorShader;

            if (!drawPlayer.active)
            {
                data.position.X = Main.playerDrawData[2].position.X;
                data.position.Y = Main.playerDrawData[2].position.Y - 14;
            }
            Main.playerDrawData.Add(data);
        });

        public override void ModifyDrawLayers(List<PlayerLayer> layers)
        {
            if (player.active)
            {
                for (int i = 0; i < layers.Count - 1; i++)
                {
                    if (layers[i].Name == "Legs")
                    {
                        layers.Insert(++i, VillagerRobeLegs);
                    }
                }
            }
            else
            {
                layers.Insert(layers.Count, VillagerRobeLegs);
            }
        }
    }
}
