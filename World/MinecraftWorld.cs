﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.GameContent.Generation;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.World.Generation;

namespace Minecraft.World
{
    class MinecraftWorld : ModWorld
    {
        public static int VillageMaxAttempts = 100000;
        public const int VillageFoundation = 8;

        public override void PostWorldGen()
        {
            GenerateVillages();
        }

        private void GenerateVillages()
        {
            List<int> housesX = new List<int>();
            int attempts = 0;
            while (attempts++ < VillageMaxAttempts)
            {
                //Find an X coordinate for this village.
                NewXCoordinate:
                int x = WorldGen.genRand.Next(100, Main.spawnTileX - 200) + (WorldGen.genRand.Next(0, 2) == 0 ? Main.spawnTileX : 0);

                //Check if it's not already taken.
                foreach (int houseX in housesX)
                {
                    if (x > houseX - 60 && x < houseX + 60)
                    {
                        goto NewXCoordinate;
                    }
                }

                //Get the Y coordinate for the ground.
                int y = (int)Math.Ceiling(Main.worldSurface * 0.45);
                while (!IsSolid(x, y) && y < Main.worldSurface) y++;

                //Set widths and heights and check before building.
                int w = WorldGen.genRand.Next(17, 27);
                int h = WorldGen.genRand.Next(9, 11);
                    
                if (CheckForVillageBuilding(x, y, w, h))
                {
                    PlaceVillageBuilding(x, y, w, h);
                    housesX.Add(x);
                    break;
                }
            }
        }

        //Mod Helper Functions

        private void Place(int x, int y, ushort tile)
        {
            WorldGen.KillTile(x, y);
            WorldGen.PlaceTile(x, y, tile);
            Main.tile[x, y].active(true);
            Main.tile[x, y].type = tile;
            Main.tile[x, y].slope(0);
        }

        private void Break(int x, int y)
        {
            WorldGen.KillTile(x, y);
            Main.tile[x, y].active(false);
        }

        private void PlaceWall(int x, int y, ushort wall)
        {
            WorldGen.KillWall(x, y);
            WorldGen.PlaceWall(x, y, wall);
            Main.tile[x, y].wall = wall;
        }

        private void BreakWall(int x, int y)
        {
            WorldGen.KillWall(x, y);
            Main.tile[x, y].wall = 0;
        }

        private bool IsSolid(int x, int y)
        {
            Tile t = Main.tile[x, y];
            return t.active() && Main.tileSolid[t.type];
        }

        //Village Building

        private bool CheckForVillageBuilding(int x, int y, int w, int h)
        {
            //Check if the floor is level at both ends.
            if (IsSolid(x, y - 1) || !IsSolid(x, y + 1) || (Main.tile[x, y].type != TileID.Dirt && Main.tile[x, y].type != TileID.Stone)) return false;
            if (IsSolid(x + (w - 1), y - 1) || !IsSolid(x + (w - 1), y + 1) || (Main.tile[x + (w - 1), y].type != TileID.Dirt && Main.tile[x + (w - 1), y].type != TileID.Stone)) return false;
            
            //Checks for each X coordinate.
            for (int i = 0; i < w; i++)
            {
                //Check if the ceiling isn't blocked.
                if (IsSolid(x + i, y - h)) return false;
                //Check if the floor isn't too far away.
                for (int j = 0; j <= VillageFoundation; j++)
                {
                    if (IsSolid(x + i, y + j)) break;
                    if (j == VillageFoundation) return false;
                }
            }
            
            //We're going ahead with it! Fill in any floor gaps with dirt.
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < VillageFoundation; j++)
                {
                    if (IsSolid(x + i, y + j))
                    {
                        Main.tile[x + i, y + j].slope(0);
                        Main.tile[x + i, y + j].halfBrick(false);
                        break;
                    }
                    Place(x + i, y + j, TileID.Dirt);
                }
            }
            return true;
        }

        private void PlaceVillageBuilding(int x, int y, int w, int h)
        {
            //Clear the area.
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    Break(x + i, y - j);
                    BreakWall(x + i, y - j);
                }
            }

            //Stone Floor and Wooden Roof
            for (int i = 0; i < w; i++)
            {
                Place(x + i, y, TileID.Stone);
                if (i > 1 && i < (w - 2)) Place(x + i, y + 1 - h, TileID.WoodBlock);
                if (i > 0 && i < (w - 1)) Place(x + i, y + 2 - h, TileID.WoodBlock);
                Place(x + i, y + 3 - h, TileID.WoodBlock);
            }

            //Slab the ends of the Stone Floor.
            if (!IsSolid(x - 1, y)) Main.tile[x, y].halfBrick(true);
            if (!IsSolid(x + w, y)) Main.tile[x + (w - 1), y].halfBrick(true);

            //Slope the ends of the Wood Roof.
            Main.tile[x, y + 3 - h].slope(2);
            Main.tile[x + 1, y + 2 - h].slope(2);
            Main.tile[x + 2, y + 1 - h].slope(2);
            Main.tile[x + (w - 1), y + 3 - h].slope(1);
            Main.tile[x + (w - 2), y + 2 - h].slope(1);
            Main.tile[x + (w - 3), y + 1 - h].slope(1);

            //Log above Wooden Door.
            for (int j = 4; j < h - 3; j++)
            {
                Place(x + 1, y - j, TileID.LivingWood);
                Place(x + (w - 2), y - j, TileID.LivingWood);
            }

            //Place Wooden Doors.
            WorldGen.PlaceObject(x + 1, y - 1, TileID.ClosedDoor, true, 0);
            WorldGen.PlaceObject(x + (w - 2), y - 1, TileID.ClosedDoor, true, 0);

            //Place Torches.
            WorldGen.PlaceObject(x, y - 5, TileID.Torches, true, 0);
            WorldGen.PlaceObject(x + 2, y - 5, TileID.Torches, true, 0);
            WorldGen.PlaceObject(x + (w - 1), y - 5, TileID.Torches, true, 0);
            WorldGen.PlaceObject(x + (w - 3), y - 5, TileID.Torches, true, 0);

            //Place House Walls.
            for (int i = 2; i < w - 2; i++)
            {
                for (int j = 1; j < h - 3; j++)
                {
                    if (i == 2 || i == w - 3)
                    {
                        PlaceWall(x + i, y - j, WallID.LivingWood);
                    }
                    else
                    {
                        PlaceWall(x + i, y - j, j == 1 ? WallID.Stone : WallID.Wood);
                    }
                }
            }

            //Spawn the Villager.
            int villager = NPC.NewNPC((x + (w / 2)) * 16, y * 16, mod.NPCType("Villager"), 0, 0f, 0f, 0f, 0f, 255);
            Main.npc[villager].homeTileX = x + (w / 2);
            Main.npc[villager].homeTileY = y;
            Main.npc[villager].direction = 1;
            Main.npc[villager].homeless = true;
        }
    }
}
